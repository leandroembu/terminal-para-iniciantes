# Terminal para iniciantes

## Objetivo

Ajudar usuários de sistemas GNU/Linux a usarem a linha de comando para tarefas simples do dia-a-dia, explicando os conceitos e comandos, mostrando a eficiência e promovendo a familiaridade com o emulador de terminal.

## Público-alvo

Pessoas com pouca ou nenhuma intimidade com a linha de comando, com ou sem experiência no uso de sistemas GNU/Linux.

## O que é o shell?

SH - Shell do Unix escrito por Steve Bourne

BASH - Bourne Again Shell - Shell melhorado para substituir o SH

## Emuladores de terminal

TTY

GNOME Terminal

Konsole

XFCE Terminal

LX Terminal

### Entrando no terminal

- Abrindo o terminal
- Abrindo o terminal em pasta específica
- Apresentando o prompt
- Digitando alguns comandos
- Saindo do terminal

## Quem sou e onde estou?

- whoami e id -un
- pwd

## Apresentando a estrutura de diretórios

- FHS - Breve explicação sobre os diretórios do GNU/Linux

## Navegando no sistema de arquivos

- ls
- cd
- Caminho absoluto e caminho relativo
- cd (meu diretório home)
- cd - (diretório anterior)
- cd ~usuario (diretório home do usuário)
